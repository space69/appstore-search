var algoliasearch = require('algoliasearch');
const cfg = require('../config.json');
const  client = algoliasearch(cfg.APP_ID, cfg.APP_KEY);
var index = client.initIndex( cfg.APP_INDEX);



module.exports = function (req, res) {
  if (req.method == 'POST') {
    console.log("POST");
    var body = '';
    req.on('data', function (data) {
      body += data;
      // console.log("Partial body: " + body);
    });
    req.on('end', function () {
      const newAppReq=JSON.parse(body);
      if(typeof newAppReq!='undefined'){
        index.addObjects([newAppReq], function(err, result) {
          console.log();
          if(typeof result.objectIDs[0] !='undefined'|| result.objectIDs[0]!= null){
            res.writeHead(200, {'Content-Type': 'application/json'});
            var json = JSON.stringify({
              id: result.objectIDs[0],
            });
            res.end(json);
          }

        })
      }else{
        res.writeHead(405, {'Content-Type': 'text/html'});
        res.end("Wrong app JSON object provided");

      }

    });

  }
  else
  {
    res.writeHead(405, {'Content-Type': 'text/html'});
    res.end("Only POST method supported");
  }
};
