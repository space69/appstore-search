var algoliasearch = require('algoliasearch');
const cfg = require('../config.json');
const  client = algoliasearch(cfg.APP_ID, cfg.APP_KEY);
var index = client.initIndex( cfg.APP_INDEX);



module.exports = function (req, res) {
  if (req.method == 'DELETE') {
    console.log("DELETE");
    const splittedURL=req.url.split('/');
    const appIDTODelete=splittedURL[splittedURL.length-1];
    const isNum = /^\d+$/.test(appIDTODelete);
    if(isNum){
      index.deleteObject(appIDTODelete, function(err) {
        if (!err) {
          res.writeHead(202, {'Content-Type': 'text/html'});
          res.end(appIDTODelete+" deleted");
        }else{
          res.writeHead(400, {'Content-Type': 'text/html'});
          res.end("Error from our Algolia's server");
        }
      });
    }else{
      res.writeHead(400, {'Content-Type': 'text/html'});
      res.end("Url should be /api/1/apps/:id, id must be an number");
    }
  }
  else
  {
    res.writeHead(405, {'Content-Type': 'text/html'});
    res.end("Only DELETE method supported");
  }
};
