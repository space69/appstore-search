var http = require('http');
var serveStaticFiles = require('ecstatic')({ root: __dirname + '/static' });
var port = process.env.PORT || 8000;
var HttpCors = require('http-cors');

// configure (all allowed - see source for options)
var cors = new HttpCors();

http.createServer(function (req, res) {
  if (cors.apply(req, res))
  return;	// this was an OPTIONS request - no further action needed
  if (req.url==='/api/1/apps') {
    return require('./lib/http-handle-ad-app')(req, res);
  }

  if (req.url.indexOf('/api/1/apps/') === 0) {
    return require('./lib/http-handle-delete-app')(req, res);
  }

  // default: handle the request as a static file
  serveStaticFiles(req, res);
}).listen(port);

console.log('Listening on http://localhost:%d', port);
