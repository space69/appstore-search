In order to use it you have to install both project(```npm install```).
Then lunch the server (from the server's root) thanks to:
```
npm start

```
and start the front app (from the front's root) thanks to:
```
yarn start
```

## Documentation
This app use React.JS with Redux. It mainly use the official react module provided by (Algolia)[https://www.algolia.com/doc/api-client/javascript/].
This web page is build over [React Static Boilerplate](https://github.com/kriasoft/react-static-boilerplate), this Boilerplate is really powerfull and we could use a smaller one. The further improvements could be done quickly thanks to the tools it provides.

**Features**

> The goal of this test is to evaluate your ability:
> * to architecture a small app,
> * to configure an Algolia index,
> * and to build an application displaying an outstanding instant-search interface.

* Use the most important algolia's concepts such as index, hit, facet, replica ....
* Aims at being fully reactive and responsive (using react material design light's grid). Application's grid is thought for different viewport, need to do the same for other component.
* Using Material design good practice (colors, no conflict with z-index, clear, robust and specific).

## Futur improvements
* We could build a Swagger documentation in order to help futur developpers (enhance the very minimalist backend by handling more use of case).
* Enhance the UX and accessibility (ARIA).
* We have just achieved some functionnal test, we must improve them.
* Use the full power of the boilerplate with new features or use a simpler one.
* Enhance the design of the web page. (we could display a sort action element in a overlay and highlight which sort is done, change flavicon, mobility, may stop using react mdl and use another one).
* Add some new features like an export one (almost already done).
* Add a behaviour to the view button like redirection, selection, buy it.
* Add new order features thanks to lodash and refinement.
* Refactor and externalize each styling objects.
* Use a more complete framwork such as express.


### Naming convention

> Many programs have variables that contain computed values: totals, averages, maximums, and so on. If you modify a name with a qualifier like Total, Sum, Average, Max, > Min, Record, String, or Pointer, put *the modifier at the end of the name*.
> ...
> This practice offers several advantages. First, *the most significant part of the variable name*, the part that gives the variable most of its meaning, is at the *front* , so it’s most prominent and gets read first. Second, by establishing this convention, you avoid the confusion you might create if you were to use both totalRevenue and revenueTotal in the same program. The names are semantically equivalent, and the convention would pre-vent their being used as if they were different. Third, a set of names like revenueTotal, expenseTotal, revenueAverage, and expenseAverage has a pleasing symmetry.

> [Code Complete 2 by Steve McConnell](http://www.stevemcconnell.com/cc.htm)
