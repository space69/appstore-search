/**
* React Static Boilerplate
* https://github.com/kriasoft/react-static-boilerplate
*
* Copyright © 2015-present Kriasoft, LLC. All rights reserved.
*
* This source code is licensed under the MIT license found in the
* LICENSE.txt file in the root directory of this source tree.
*/

import React, { PropTypes } from 'react';
import Layout from '../../components/Layout';
import s from './styles.css';
import cx from 'classnames';
import { title } from './index.md';

import { InstantSearch, Hits, SearchBox, Highlight, RefinementList, CurrentRefinements, ClearAll, Pagination, SortBy } from 'react-instantsearch/dom';
import { Card, CardTitle, CardText, CardActions, Button, Chip, Grid, Cell, IconButton, Menu, MenuItem } from 'react-mdl';
const cfg = require('../../config.json');

// First, we need to add the Hits component to our import
// Component tha could be externalize and which wrap the hit=result or element stored on algolia's backend
// Min height is use in order to have card with same size despite the content.
function Application({ hit }) {
  return (
    <Cell col={4} tablet={4} phone={4} shadow={2} style={{ display: 'inline-block' }}>
      <Card shadow={0} style={{ width: '90%' }}>
        <CardTitle expand style={{ color: '#fff', background: `url(${hit.image}) center no-repeat #46B6AC`, backgroundSize: 'contain' }}>{hit.price}</CardTitle>
        <CardText style={{ minHeight: '5em' }}>
          <Highlight attributeName="name" hit={hit} /><br />
          {hit.rating}          ★         ({hit.ratingCount}          )
        </CardText>
        <CardActions border>
          <Button colored>View</Button>
          <Chip style={{ float: 'right' }}>{hit.category}</Chip>
        </CardActions>
      </Card>
    </Cell>

  );
}

// Should be refacto. Using Grid is always a best practise and allows us to think about mobility
// TODO Externalize style
// TODO Continue the accessiblity thanks to ARIA

function Search() {
  return (
    <div className="container">

      <Grid className="search-controls">
        <Cell col={7} phone={4}>
          <SearchBox aria-required required autofocus aria-label="Search Box" />
        </Cell>
        <Cell col={3} phone={2} offsetPhone={2}>
          <div style={{ position: 'relative' }}>
            <IconButton name="more_vert" id="demo-menu-lower-right" style={{ float: 'right' }} />
            <Menu target="demo-menu-lower-right" align="right">
              <RefinementList attributeName="category" />
            </Menu>
          </div>
        </Cell>
        <Cell col={2} phone={4}>
          <SortBy
            items={[
           { value: 'applications_by_rating_desc', label: 'Rating desc.' },
           { value: 'applications_by_rating_asc', label: 'Rating asc.' },
            ]}
            defaultRefinement="apps"
          />
        </Cell>
        <Cell col={12}>
          <CurrentRefinements />
          <ClearAll style={{ margin: '1em' }} />
        </Cell>
      </Grid>
      <Grid className="applications">

        <Cell col={12}>
          <Hits hitComponent={Application} />
        </Cell>
        <Cell col={12} style={{ textAlign: 'center' }}>
          <Pagination style={{ width: 'auto', margin: 'auto' }} />
        </Cell>
      </Grid>

    </div>
  );
}


// Main container that provide each Component

class HomePage extends React.Component {

  componentDidMount() {
    document.title = title;
  }

  render() {
    return (
      <div className={cx(s.content, 'body')}>
        <Layout>
          <InstantSearch
            appId={cfg.APP_ID}
            apiKey={cfg.APP_KEY}
            indexName={cfg.APP_INDEX}
          >
            <Search />
          </InstantSearch>
        </Layout>
      </div>
    );
  }

          }

export default HomePage;
